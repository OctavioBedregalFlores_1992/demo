package com.example.demo.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
class TestController {
    @RequestMapping("/")
    @ResponseBody
    public String hello() {
        return "Hello Spring Boot";
    }
    @RequestMapping("/hello")
    @ResponseBody
    public String hello2() {
        return "Hello 2 Spring Boot";
    }
    @RequestMapping("/demo")
    @ResponseBody
    public String demo() {
        return "Hello 3 Spring Boot";
    }
}